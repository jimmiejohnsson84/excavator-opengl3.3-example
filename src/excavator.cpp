// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
using namespace std;

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

#include "objloader.h"
#include "excavator.h"

Excavator::Excavator()
{
	m_vertexBufferPlatform = 0;
	m_normalBufferPlatform = 0;

	m_vertexBufferWheels = 0;
	m_normalBufferWheels = 0;

	m_vertexBufferPlatformSize = 0;
	m_vertexBufferWheelsSize = 0;

	m_platformRotationY = 0;
	m_wheelsRotationY = 0;
	m_pos = glm::vec3(0, 0, 0);
 }

Excavator::~Excavator()
{
	if(m_built)
	{
		glDeleteBuffers(1, &m_vertexBufferPlatform);
		glDeleteBuffers(1, &m_normalBufferPlatform);

		glDeleteBuffers(1, &m_vertexBufferWheels);
		glDeleteBuffers(1, &m_normalBufferWheels);
	}
}

 bool Excavator::BuildExcavator()
 {
	bool retValue = true;

	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;

	std::vector<glm::vec3> verticesWheels;
	std::vector<glm::vec2> uvsWheels;
	std::vector<glm::vec3> normalsWheels;

	// Load Platform	
	retValue = loadOBJ("Excavator_base.obj", vertices, uvs, normals);	
	if(retValue)
	{
		// Load Wheels
		retValue = loadOBJ("Excavator_legs.obj", verticesWheels, uvsWheels, normalsWheels);
	}

	if(retValue)
	{
		// Build Platform
		glGenBuffers(1, &m_vertexBufferPlatform);
		glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferPlatform);
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

		glGenBuffers(1, &m_normalBufferPlatform);
		glBindBuffer(GL_ARRAY_BUFFER, m_normalBufferPlatform);
		glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);		

		// Build Wheels
		glGenBuffers(1, &m_vertexBufferWheels);
		glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferWheels);
		glBufferData(GL_ARRAY_BUFFER, verticesWheels.size() * sizeof(glm::vec3), &verticesWheels[0], GL_STATIC_DRAW);

		glGenBuffers(1, &m_normalBufferWheels);
		glBindBuffer(GL_ARRAY_BUFFER, m_normalBufferWheels);
		glBufferData(GL_ARRAY_BUFFER, normalsWheels.size() * sizeof(glm::vec3), &normalsWheels[0], GL_STATIC_DRAW);
		
	}

	m_vertexBufferPlatformSize = vertices.size();
	m_vertexBufferWheelsSize = verticesWheels.size();
	m_built = retValue;

	return retValue;
 }

 void Excavator::RenderPlatform(GLuint MatrixID, GLuint ViewMatrixID, GLuint ModelMatrixID, GLuint MaterialDiffuseColorID, 
 									glm::mat4 &projection, glm::mat4 &view)
 {
	// Create a model matrix that rotates platform around the Y-axis and then translate them to the world position m_pos
	glm::mat4 model = glm::mat4(1.0f);
	glm::mat4 rotateBase = glm::rotate(model, glm::radians(m_platformRotationY), glm::vec3(0, 1, 0));
	glm::mat4 translateBase = glm::translate(model, m_pos);
	model = translateBase * rotateBase;	
	glm::mat4 modelViewProjection = projection * view * model;

	// Forward to shader
	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &modelViewProjection[0][0]);	
	glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &model[0][0]);
	glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &view[0][0]);		

	// Give the whole platform a beige orange color
	glm::vec3 materialDiffuseColor(0.6f, 0.53f ,0.36f);
	glUniform3f(MaterialDiffuseColorID, materialDiffuseColor.x, materialDiffuseColor.y, materialDiffuseColor.z);		

	// Enable vertex & normal attribute arrays		
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferPlatform);
	glVertexAttribPointer(
		0,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		(void*)0
	);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, m_normalBufferPlatform);
	glVertexAttribPointer(
		1,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		(void*)0
	);		

	// Render the platform
	glDrawArrays(GL_TRIANGLES, 0, m_vertexBufferPlatformSize);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);		
 }

void Excavator::RenderWheels(GLuint MatrixID, GLuint ViewMatrixID, GLuint ModelMatrixID, GLuint MaterialDiffuseColorID, 
								glm::mat4 &projection, glm::mat4 &view)
 {
	// Create a model matrix that rotates wheels around the Y-axis and then translate them to the world position m_pos
	glm::mat4 model = glm::mat4(1.0f);
	glm::mat4 rotate = glm::rotate(model, glm::radians(m_wheelsRotationY), glm::vec3(0, 1, 0));
	glm::mat4 translate = glm::translate(model, m_pos);
	model = translate * rotate;
	glm::mat4 modelViewProjection = projection * view * model;

	// Forward to shader
	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &modelViewProjection[0][0]);	
	glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &model[0][0]);
	glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &view[0][0]);		

	// Give wheels a grey color
	glm:vec3 materialDiffuseColor(0.4f,0.4f ,0.4f);
	glUniform3f(MaterialDiffuseColorID, materialDiffuseColor.x, materialDiffuseColor.y, materialDiffuseColor.z);				

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferWheels);
	glVertexAttribPointer(
		0,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		(void*)0
	);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, m_normalBufferWheels);
	glVertexAttribPointer(
		1,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		(void*)0
	);		
	
	glDrawArrays(GL_TRIANGLES, 0, m_vertexBufferWheelsSize);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);	
 }

void Excavator::RenderExcavator(GLuint MatrixID, GLuint ViewMatrixID, GLuint ModelMatrixID, GLuint LightID, GLuint MaterialDiffuseColorID)
{
	if(m_built)
	{
		// Setup lighting, a single spot light
		glm::vec3 lightPos = glm::vec3(4,4,4);
		glUniform3f(LightID, lightPos.x, lightPos.y, lightPos.z);

		// Setup projection, view and model matrix
		glm::mat4 projection = glm::perspective(glm::radians(45.0f), (float) 4.0f / (float) 3.0f, 0.1f, 100.0f);	
		glm::mat4 view = glm::lookAt(
			glm::vec3(0,5,5), 
			glm::vec3(0,0,0), 
			glm::vec3(0,1,0)
			);

		RenderPlatform(MatrixID, ViewMatrixID, ModelMatrixID, MaterialDiffuseColorID, projection, view);
		RenderWheels(MatrixID, ViewMatrixID, ModelMatrixID, MaterialDiffuseColorID, projection, view);
	}
}

float Excavator::PlatformRotationY() const
{
	return m_platformRotationY;
}

void Excavator::PlatformRotationY(float rotY)
{
	m_platformRotationY = rotY;

	if(m_platformRotationY >= m_wheelsRotationY + 45.0f)
	{
		m_platformRotationY = m_wheelsRotationY + 45.0f;
	}
	else if(m_platformRotationY <= m_wheelsRotationY - 45.0f)
	{
		m_platformRotationY = m_wheelsRotationY - 45.0f;
	}
}

float Excavator::WheelsRotationY() const
{
	return m_wheelsRotationY;
}

void Excavator::WheelsRotationY(float rotY)
{
	m_wheelsRotationY = rotY;

	if(m_wheelsRotationY >= m_platformRotationY + 45.0f)
	{
		m_wheelsRotationY = m_platformRotationY + 45.0f;
	}
	else if(m_wheelsRotationY <= m_platformRotationY - 45.0f)
	{
		m_wheelsRotationY = m_platformRotationY - 45.0f;
	}	
}

void Excavator::MoveForward(glm::vec3 dir)
{
	m_pos += dir;
}

void Excavator::MoveBackward(glm::vec3 dir)
{
	m_pos -= dir;
}