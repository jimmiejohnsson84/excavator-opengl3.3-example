#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "handleInput.h"
#include "excavator.h"


InputState::InputState()
{
	m_rotateWheels = false;
	m_pressedSpaceButtonFirstTime = false;
	m_pressedSpaceButtonLastTime = 0;
}

InputState::~InputState()
{

}

bool InputState::RotateWheels() const
{
	return m_rotateWheels;
}

void InputState::RotateWheels(bool rotateWheels)
{
	m_rotateWheels = rotateWheels;
}

double InputState::PressedSpaceButtonLastTime() const
{
	return m_pressedSpaceButtonLastTime;
}

void InputState::PressedSpaceButtonLastTime(double lastTime)
{
	m_pressedSpaceButtonLastTime = lastTime;
}

bool InputState::PressedSpaceButtonFirstTime() const
{
	return m_pressedSpaceButtonFirstTime;
}

void InputState::PressedSpaceButtonFirstTime(bool pressedSpaceButtonFirstTime)
{
	m_pressedSpaceButtonFirstTime = pressedSpaceButtonFirstTime;
}

bool InputState::CanSwitchRotationObject(double currentTime) const
{
	return (!m_pressedSpaceButtonFirstTime || float(currentTime - m_pressedSpaceButtonLastTime) > 0.5f);
}

void HandleKeyInput(GLFWwindow* window, InputState &inputState, double &lastTime, Excavator &excavator)
{
	// Time difference between frames
	double currentTime = glfwGetTime();
	float deltaTime = float(currentTime - lastTime);

	const float rotSpeed = 55;
	const float moveSpeed = 1;

	glfwPollEvents();
	if (glfwGetKey(window, GLFW_KEY_SPACE ) == GLFW_PRESS 
		&& inputState.CanSwitchRotationObject(currentTime))
	{
		inputState.RotateWheels(!inputState.RotateWheels());
		inputState.PressedSpaceButtonFirstTime(true);
		inputState.PressedSpaceButtonLastTime(currentTime);
	}

	if (glfwGetKey(window, GLFW_KEY_RIGHT ) == GLFW_PRESS)
	{
		if(inputState.RotateWheels())
		{
			excavator.WheelsRotationY(excavator.WheelsRotationY() + deltaTime * rotSpeed);
		}
		else
		{
			excavator.PlatformRotationY(excavator.PlatformRotationY() + deltaTime * rotSpeed);
		}
	}
	if (glfwGetKey(window, GLFW_KEY_LEFT ) == GLFW_PRESS)
	{
		if(inputState.RotateWheels())
		{
			excavator.WheelsRotationY(excavator.WheelsRotationY() - deltaTime * rotSpeed);
		}
		else
		{
			excavator.PlatformRotationY(excavator.PlatformRotationY() - deltaTime * rotSpeed);
		}
	}

	const float modelOffsetrotY = -45.0f;
	glm::vec3 dir(
		sin(glm::radians(excavator.WheelsRotationY() + modelOffsetrotY)),
		0,
		cos(glm::radians(excavator.WheelsRotationY() + modelOffsetrotY))
	);

	if (glfwGetKey(window, GLFW_KEY_UP ) == GLFW_PRESS)
	{
		excavator.MoveForward(dir * deltaTime * moveSpeed);
	}
	if (glfwGetKey(window, GLFW_KEY_DOWN ) == GLFW_PRESS)
	{
		excavator.MoveBackward(dir * deltaTime * moveSpeed);
	}

	lastTime = currentTime;
}