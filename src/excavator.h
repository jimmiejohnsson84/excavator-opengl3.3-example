#ifndef EXCAVATOR_H
#define EXCAVATOR_H


class Excavator
{
	public:
	Excavator();
	~Excavator();

	bool	BuildExcavator();
	void	RenderExcavator(GLuint MatrixID, GLuint ViewMatrixID, GLuint ModelMatrixID, GLuint LightID, GLuint MaterialDiffuseColorID);	

	float	PlatformRotationY() const;
	void	PlatformRotationY(float rotY);

	float	WheelsRotationY() const;
	void	WheelsRotationY(float rotY);

	void	MoveForward(glm::vec3 dir);
	void	MoveBackward(glm::vec3 dir);

	private:
	void	RenderPlatform(GLuint MatrixID, GLuint ViewMatrixID, GLuint ModelMatrixID, GLuint MaterialDiffuseColorID, glm::mat4 &projection, glm::mat4 &view);
	void	RenderWheels(GLuint MatrixID, GLuint ViewMatrixID, GLuint ModelMatrixID, GLuint MaterialDiffuseColorID, glm::mat4 &projection, glm::mat4 &view);
	
	// VBOs containing vertex and normal data for the platform and the wheels
	GLuint	m_vertexBufferPlatform;
	GLuint	m_normalBufferPlatform;

	GLuint	m_vertexBufferWheels;
	GLuint	m_normalBufferWheels;

	unsigned int	m_vertexBufferPlatformSize;
	unsigned int	m_vertexBufferWheelsSize;

	// Position and rotation information
	glm::vec3		m_pos;
	float			m_platformRotationY;
	float			m_wheelsRotationY;
	
	// If true, VBOs have been built
	bool			m_built;
};

#endif