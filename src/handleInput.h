#ifndef HANDLEINPUT_H
#define HANDLEINPUT_H

#include <glm/glm.hpp>

// Foward declarations
class Excavator;

class InputState
{
    public:
    InputState();
    ~InputState();

    bool    RotateWheels() const;
    void    RotateWheels(bool rotateWheels);

    double  PressedSpaceButtonLastTime() const;
    void    PressedSpaceButtonLastTime(double lastTime);

    bool    PressedSpaceButtonFirstTime() const;
    void    PressedSpaceButtonFirstTime(bool pressedSpaceButtonFirstTime);

    bool    CanSwitchRotationObject(double currentTime) const;

    private:
    bool    m_rotateWheels;
    double  m_pressedSpaceButtonLastTime;
    bool    m_pressedSpaceButtonFirstTime;            
    
};

void HandleKeyInput(GLFWwindow* window, InputState &inputState, double &lastTime, Excavator &excavator);

#endif