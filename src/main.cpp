// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
using namespace std;

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

#include "shader.h"
#include "initGL.h"
#include "handleInput.h"
#include "excavator.h"

void DisplayStartupText()
{
	cout << "Excavator OpenGL example" << endl;
	cout << "This program lets you control an Excavator. The Excavator consists of two parts, the wheels and the body (platform)." << endl;
	cout << "The wheels and the platform can rotate independently of eachother, but they can only pivot +/-45 degrees from eachother." << endl;
	cout << "The excavator can move forward and backward." << endl;
	cout << "*** CONTROLS ***" << endl;
	cout << "Up/Down - Move the excavator forward or backward" << endl;
	cout << "Left/Right - Rotate the platform or the wheels" << endl;
	cout << "Space - Switch between rotating platform and wheels" << endl;
	cout << "Esc - Exit" << endl;
	cout << "*** CONTROLS ***" << endl;
	cout << "Press enter to continue ";
}

int main( void )
{
	GLFWwindow* window;

	DisplayStartupText();
	getchar(); // Wait for user to press Enter

	if(InitGL(&window) == -1)
	{
		return -1;
	}

	GLuint vertexArrayID;
	glGenVertexArrays(1, &vertexArrayID);
	glBindVertexArray(vertexArrayID);

	// Create and compile our GLSL program from the shaders
	GLuint programID = LoadShaders( "TransformVertexShader.vertexshader", "SimpleFragmentShader.fragmentshader" );	

	// Get handles for data in the shader
	GLuint MatrixID = glGetUniformLocation(programID, "MVP");
	GLuint ViewMatrixID = glGetUniformLocation(programID, "V");
	GLuint ModelMatrixID = glGetUniformLocation(programID, "M");	
	GLuint LightID = glGetUniformLocation(programID, "LightPosition_worldspace");
	GLuint MaterialDiffuseColorID = glGetUniformLocation(programID, "MaterialDiffuseColor");

	// Set background color
	glClearColor(0.0f, 0.0f, 0.5f, 0.0f);		

	InputState inputState;
	Excavator theExcavtor;
	if(!theExcavtor.BuildExcavator())
	{
		cout << "Unable to load one of the .obj files, are you running program in correct working directory?" << endl;
		return -1;
	}
	double lastTime = glfwGetTime();

	// Use our shader
	glUseProgram(programID);
	while(glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS && glfwWindowShouldClose(window) == 0)
	{
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		theExcavtor.RenderExcavator(MatrixID, ViewMatrixID, ModelMatrixID, LightID, MaterialDiffuseColorID);
		HandleKeyInput(window, inputState, lastTime, theExcavtor);		

		glfwPollEvents();
		glfwSwapBuffers(window);		
	}

	// Clean up and close OpenGL
	glDeleteVertexArrays(1, &vertexArrayID);	
	glfwTerminate();

	return 0;
}

