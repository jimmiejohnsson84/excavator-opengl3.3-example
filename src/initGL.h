#ifndef INITGL_H
#define INITGL_H

// Forward declarations
struct GLFWwindow;

int InitGL(GLFWwindow** window);

#endif