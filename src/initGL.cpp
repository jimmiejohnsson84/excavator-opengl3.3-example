#include <iostream>
using namespace std;

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "initGL.h"

int InitAndSetHints()
{
	// Initialise GLFW
	if( !glfwInit() )
	{
		cout << "Failed to initialize GLFW" << endl;
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	return 1;
}

int CreateglfwWindowAndMakeCurrent(GLFWwindow** window)
{
	// Open a window and create its OpenGL context
	*window = glfwCreateWindow( 1024, 768, "Excavator", NULL, NULL);
	if( *window == NULL )
	{
		cout << "Failed to open GLFW window" << endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(*window);
    
    return 1;
}

int InitGlewAndInputMode(GLFWwindow* window)
{
	// Initialize GLEW
	glewExperimental = true;
	if (glewInit() != GLEW_OK) 
	{
		cout << "Failed to initialize GLEW" << endl;
		glfwTerminate();
		return -1;
	}

	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
    
    return 1;
}

int InitGL(GLFWwindow** window)
{
	if(InitAndSetHints() == -1)
	{
		return -1;
	}

	if(CreateglfwWindowAndMakeCurrent(window) == -1)
	{
		return -1;
	}

	if(InitGlewAndInputMode(*window) == -1)
	{
		return -1;
	}

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS); 

	// Enable culling
	glEnable(GL_CULL_FACE);
	

	return 1;	
}